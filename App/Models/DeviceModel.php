<?php
/**
 * Created by PhpStorm.
 * User: SimonaThrussell
 * Date: 07/11/2018
 * Time: 13:15
 */

namespace App\Models;
use PDO;

class DeviceModel extends \Core\Model
{

    public static function makeDevice($order_id,$devicetype,$devicestorage, $deviceconnection,$devicecondition,$devicecolour)
    {
        try{
            $db=static::getDB();
            $sql= "INSERT into
            forzaerp_rebuy_order_device (`order_id`,`device_type_id`,`device_storage_id`,`device_connection_id`,`device_condition_id`,`device_colour_id`) 
            VALUES (?,?,?,?,?,?)";
            $stmt=$db->prepare($sql);
            $stmt->execute([$order_id,$devicetype,$devicestorage, $deviceconnection,$devicecondition,$devicecolour

            ]);
            $stmt = null;

            $message="Device created";
            return $message;

        }
        catch (\PDOException $e)
        {
            echo $e->getMessage();
        }


    }





}