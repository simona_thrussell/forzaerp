<?php
/**
 * Created by PhpStorm.
 * User: darke
 * Date: 31/10/2018
 * Time: 20:04
 */

namespace App\Controllers;
use \Core\View;
use \Core\Customer;
use \Core\Order;
use App\Models\RMAModel;

require 'C:\xampp\htdocs\FORZAERP\vendor\autoload.php';
class RMA extends \Core\Controller
{
    public function indexAction()
    {

        //$results = RebuyModel::getOrders();
        View::renderTemplate('RMA/index.html'
        //, [
        //'results' => $results
        //]
        );

    }

    public function reportsAction()
    {

        //$results = RebuyModel::getOrders();
        View::renderTemplate('RMA/reports.html'
        //, [
        //'results' => $results
        //]
        );

    }

    public function inspectionAction()
    {

        //$results = RebuyModel::getOrders();
        View::renderTemplate('RMA/inspection.html'
        //, [
        //'results' => $results
        //]
        );

    }

    public function repairAction()
    {

        //$results = RebuyModel::getOrders();
        View::renderTemplate('RMA/repair.html'
        //, [
        //'results' => $results
        //]
        );

    }



    public function inventoryAction()
    {

        //$results = RebuyModel::getOrders();
        View::renderTemplate('RMA/inventory.html'
        //, [
        //'results' => $results
        //]
        );

    }

    public function mailAction()
    {

        //$results = RebuyModel::getOrders();
        View::renderTemplate('RMA/mail.html'
        //, [
        //'results' => $results
        //]
        );

    }


}