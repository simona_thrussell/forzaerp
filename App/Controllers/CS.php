<?php
/**
 * Created by PhpStorm.
 * User: SimonaThrussell
 * Date: 01/10/2018
 * Time: 09:30
 */

namespace App\Controllers;
use \Core\View;
use App\Models\CustomerSupportModel;

class CS extends \Core\Controller
{
    public function indexAction()
    {

        $results = CustomerSupportModel::getOrders();
        View::renderTemplate('CS/index.html'
            //['results'=>$results]
        );

    }
}