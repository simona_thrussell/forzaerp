<?php
/**
 * Created by PhpStorm.
 * User: simona
 * Date: 28/09/2018
 * Time: 16:57
 */

namespace App\Controllers;
use \Core\View;
use \Core\Customer;
use \Core\Order;
use \Core\Device;
use App\Models\RebuyModel;
use App\Models\CustomerModel;
use App\Models\OrderModel;
use App\Models\DeviceModel;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'C:\xampp\htdocs\FORZAERP\vendor\autoload.php';
class Rebuy extends \Core\Controller
{
    public function indexAction()
    {


        $results2=rebuyModel::getStatus();
        View::renderTemplate('Rebuy/index.html', [

            'results2'=> $results2
        ]);

    }

    public function activateAction()
    {
        $id = $this->route_params['id'];
        return $id;
    }

    public function latestordersAction()
    {
        $results = RebuyModel::getOrders();
        View::renderTemplate('Rebuy/latestorders.html', [
            'results' => $results,

        ]);

    }

    public function inspectionAction()
    {
        $results = RebuyModel::getInspection();
        View::renderTemplate('Rebuy/inspection.html', [
            'results' => $results
        ]);

    }

    public function inspectAction()
    {
        $order_id=$this->activateAction();
        $results = RebuyModel::getDevice($order_id);
        View::renderTemplate('Rebuy/inspect.html', [
            'results' => $results
        ]);
        $_SESSION['order_id']=$order_id;

    }

    public function editAction()
    {
        $results = RebuyModel::getOrders();
        View::renderTemplate('Rebuy/edit.html', [
            'results' => $results
        ]);
    }

    public function mailaction()
    {

        $results = RebuyModel::getOrders();
        View::renderTemplate('Rebuy/mail.html', [
            'results' => $results
        ]);
        //use php mailer script in rebuy/mail.php

    }

    public function sendmailAction()
    {
        $order_id=$this->activateAction();
            $results = RebuyModel::getDevice($order_id);
            View::renderTemplate('Rebuy/sendmail.html', [
                'results' => $results
            ]);

    }

    public function inspectsubmitAction()

    {
        $order_id=$_SESSION['order_id'];
        //$order_id=$this->activateAction();
        $results = RebuyModel::getDevice($order_id);

        View::renderTemplate('Rebuy/inspectsubmit.html',[
            'results'=>$results

            ]
            );




        $devicetype = $_POST['device_type'];
        $devicestorage = $_POST['device_storage'];
        $deviceconnection = $_POST['device_connection'];
        $devicecondition = $_POST['device_condition'];
        $devicecolour = $_POST['device_colour'];
        //$images=$_POST['images'];
        $devicecomments = $_POST['device_comments'];
        $query = RebuyModel::InspectSubmit($order_id, $devicetype, $devicestorage, $deviceconnection, $devicecondition, $devicecolour, $devicecomments);
        $status=RebuyModel::updateFStatus(16,$order_id);


        //$failcard=$_POST['failcard'];
        if (!isset($_POST['failcard'])) {


            echo "no parts require  replacement";

        } else {
            echo '<pre>';
            //var_dump($_POST['failcard']);
            echo "See below for parts required";
            $failcard = $_POST['failcard'];
            echo '<pre>';
            echo '<h3>Parts Needed </h3></br>';
            foreach ($failcard as $value) {
                echo $value . "</br>";
            }
        }




    }

    public function statusAction()
    {
        $results = RebuyModel::getStatus();
        View::renderTemplate('Rebuy/status.html', [
            'results' => $results
        ]);

    }







    public function mailsentAction()
    {
        $order_id=$this->activateAction();
        View::renderTemplate('Rebuy/mailsent.html');
        require 'C:\xampp\htdocs\rebuyold\vendor\autoload.php';
        if(isset($_POST['submit'])) {
          $name=$_POST['name'];
          $email=$_POST['email'];
          $subject=$_POST['title'];
          $body=$_POST['msg'];
            echo '<pre>';
            var_dump($_POST);

            $mail = new PHPMailer(TRUE);

            try {

                $mail->setFrom('simona.thrussell@forza-refurbished.nl', $name);
                $mail->addAddress('sdthrussell@gmail.com', 'your name');
                $mail->Subject = $subject;
                $mail->Body = $body;

                /* SMTP parameters. */
                $mail->isSMTP();
                $mail->Host = 'smtp.office365.com';
                $mail->SMTPAuth = TRUE;
                $mail->SMTPSecure = 'tls';
                $mail->Username = 'simona.thrussell@forza-refurbished.nl';
                $mail->Password = 'DcadkA7h';
                $mail->Port = 587;

                /* Disable some SSL checks. */
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );

                /* Finally send the mail. */
                $mail->send();
                $status=RebuyModel::updateFStatus(9,$order_id);
            }
            catch (Exception $e)
            {
                echo $e->errorMessage();
            }



            }
            else{

            echo "no input sumitted";

        }

    }

    public function checkordersAction()
    {
        $results = RebuyModel::getCheck();
        View::renderTemplate('Rebuy/checkorders.html', [
            'results' => $results
        ]);


    }

    public function checkAction()
    {
        $order_id=$this->activateAction();
        $results = RebuyModel::getDevice($order_id);
        View::renderTemplate('Rebuy/check.html');
    }

    public function checksubmitAction()
    {
        $order_id=$this->activateAction();
        View::renderTemplate('Rebuy/checksubmit.html');

        echo '<pre>';
        //var_dump($_POST['check']);
        if(isset($_POST['check'])) {
            $check[] = $_POST['check'];
        }


        if (empty($_POST['IMEI'])) {

            die("IMEI not present. Please go back and enter it.");


        } else {
          $validate=Device::validateIMEI($_POST['IMEI']);


        }

        if (empty($_POST['check'])) {
            echo "No options were checked";


        } else {
            $check = $_POST['check'];


            $N = count($check);
            if ($N == 3) {
                echo "check passed!";
                $checked=true;

            }else {
                echo "the device did not pass check, please see notes.";
                $checked=false;


            }
            if(isset($IMEI)) {
                if($checked==true) {
                    $query = RebuyModel::checksubmit($IMEI, $checked, $order_id);
                    $status=RebuyModel::updateFStatus(4,$order_id);
                }
            };






        }





    }

    public function reportsAction()
    {
        View::renderTemplate('Rebuy/reports.html'
            //, [
          //  'results' => $results
       //]
        );

    }

    public function quoteAction()
    {
        $order_id=$this->activateAction();
        $results = RebuyModel::getDevice($order_id);
        View::renderTemplate('Rebuy/quote.html', [
            'results' => $results
        ]);

    }


    public function quoteordersAction()
    {
        $results = RebuyModel::getOrders();
        View::renderTemplate('Rebuy/quoteorders.html', [
            'results' => $results
        ]);

    }


    public function submitquoteAction()
    {
        $order_id=$this->activateAction();
       $quote = $_POST['quote'];
        $query = RebuyModel::SubmitQuote($order_id,$quote);
        $status=RebuyModel::updateFStatus(17,$order_id);
       // $results = RebuyModel::getOrders();
        View::renderTemplate('Rebuy/submitquote.html'
            //, [
           // 'results' => $results
       // ]
    );


    }

    public function overviewAction()
    {

        $results = RebuyModel::Overview();
        View::renderTemplate('Rebuy/overview.html', [
         'results' => $results
         ]
        );

    }


    public function enterorderAction()
    {

        View::renderTemplate('Rebuy/enterorder.html');




    }

    public function entersubmitAction()
    {

        View::renderTemplate('Rebuy/entersubmit.html');

        $devicetype = $_POST['device_type'];
        $devicestorage = $_POST['device_storage'];
        $deviceconnection = $_POST['device_connection'];
        $devicecondition = $_POST['device_condition'];
        $devicecolour = $_POST['device_colour'];

        $firstname=$_POST['first_name'];
        $lastname=$_POST['last_name'];
        $email=$_POST['email'];
        $phone=$_POST['phone'];
        $customer_type=$_POST['customer_type'];
        $postcode=$_POST['postcode'];
        $streetnumber=$_POST['street_number'];
       $addition=$_POST['addition'];
       $streetname=$_POST['street_name'];
        $city=$_POST['city'];
      $country=$_POST['country'];

        $paymenttype=$_POST['payment'];
        $IBAN=$_POST['IBAN'];
        $Tnv=$_POST['Tnv'];


       $customer_id= CustomerModel::createCustomer($firstname,$lastname,$email,$phone, $customer_type);
        echo '<pre>';
       echo $customer_id;
       $query=CustomerModel::createAddress($customer_id,$postcode,$streetnumber,$addition,$streetname,$city,$country);
       // RebuyModel::OrderSubmit($order_id, $devicetype, $devicestorage, $deviceconnection, $devicecondition, $devicecolour);
        echo $query;
        $order_id=OrderModel::createOrder($customer_id,$paymenttype);
       echo $order_id;
       //var_dump($_POST);
      echo  $device=DeviceModel::makeDevice($order_id,$devicetype,$devicestorage,$deviceconnection,$devicecondition,$devicecolour);
      echo $status=RebuyModel::makeStatus($order_id)."</br>";
      echo $shipping=RebuyModel::createShippingStatus($order_id);

    }

    public function serviceAction()
    {
        View::renderTemplate('Rebuy/service.html');

    }

     public function setstatusAction()
     {
         $order_id=$this->activateAction();
         $results=RebuyModel::setStatus($order_id);
         View::renderTemplate('Rebuy/setstatus.html', [
             'results' => $results
         ]);
         echo '<pre>';
         var_dump($results);
        foreach($results as $result){
          $co=$results["customer_order_status"];
          $fo=$results["forza_order_status"];
          echo $co;
          echo $fo;




        }


     }

    public function acceptQuoteAction()
    {
        $order_id=$this->activateAction();
        View::renderTemplate('Rebuy/acceptquote.html');
        $_SESSION['order_id']=$order_id;

        $status=RebuyModel::updateCStatus(4,$order_id);

    }


    public function confirmAction()
    {
        //$order_id=$this->activateAction();
        $order_id=$_SESSION['order_id'];
        View::renderTemplate('Rebuy/confirm.html');
        var_dump($_POST);
        if(isset($_POST['accept_button']))
        {
            RebuyModel::acceptQuote($order_id);
        }elseif(isset($_POST['refuse_button']))
        {
            RebuyModel::refuseQuote($order_id);

        }


    }


    public function addtagsAction()
    {

        $results = RebuyModel::Overview();
        View::renderTemplate('Rebuy/addtags.html', [
                'results' => $results
            ]
        );


    }

    public function setStatus()
    {
        //$order_id=$this->activateAction();
        $results = RebuyModel::getStatus();

        foreach ($results as $result) {
            $order_id = $result['order_id'];
            $cstatus = $result['customer_order_status'];
            $fstatus = $result['status_id'];
            $action = $result['next_action_id'];
            $update=self::setAction($cstatus,$fstatus);



        }
        View::renderTemplate('Rebuy/setstatus.html', [
                'results' => $results
            ]
        );
    }

    public function setAction($cstatus,$fstatus)
    {


        //$action;
        $status = RebuyModel::setAction($action, $order_id);
    }


    public function setQuote()
    {




    }

    public function setfailcard($order_id)
    {
        if(isset($_POST['failcard']))
        {




        }



    }

    public function devicereceivedAction()
    {
        $results = RebuyModel::Shipping();
        View::renderTemplate('Rebuy/devicereceived.html', [
                'results' => $results
            ]
        );


    }

     public function ordereditAction()
     {
         $order_id=$this->activateAction();
         $results = RebuyModel::GetOrders();
         View::renderTemplate('Rebuy/devicereceived.html', [
                 'results' => $results
             ]
         );

     }

     public function shippingAction()
     {
         $results = RebuyModel::getShipping();
         View::renderTemplate('Rebuy/shipping.html', [
                 'results' => $results
             ]
         );



     }

     public function editshippingAction()
     {
         $order_id=$this->activateAction();
         $_SESSION['order_id']=$order_id;
         $results = RebuyModel::getShippingById($order_id);
         View::renderTemplate('Rebuy/editshipping.html', [
                 'results' => $results
             ]
         );


     }

     public function shippingupdateAction()
     {
         //$order_id=$this->activateAction();
         $order_id=$_SESSION['order_id'];
         if(isset($_POST['submit'])) {
             //var_dump($_POST['submit']);
             $status = $_POST['status'];
             $query=RebuyModel::updateShipping($status, $order_id);

             switch($status) {
                 case 3:
                     $fstatus = 3;
                     $query = RebuyModel::updateFStatus(3, $order_id);
                     break;

                 case 7:
                     $action = 1;
                     $query = RebuyModel::setAction(1, $order_id);
                     break;

             }

         }
         else{

             echo "no info was sent";
         }
         $results = RebuyModel::getShippingById($order_id);
         View::renderTemplate('Rebuy/shippingupdate.html', [
                 'results' => $results
             ]
         );


     }


     public function secondOfferAction()
     {
         $results = RebuyModel::GetRefusedOffers();
         View::renderTemplate('Rebuy/secondOffer.html', [
             'results' => $results
         ]);



     }

     public function submitsecondquoteAction()
     {
         $order_id=$_SESSION['order_id'];
         $results=RebuyModel::retrievesecondquote($order_id);
         View::renderTemplate('Rebuy/submitsecondquote.html',[
             'results'=>$results

         ]);
         $order_id=$_SESSION['order_id'];
        // var_dump($_POST);
         if(isset($_POST['submit'])) {
             $quote=$_POST['quote'];
            echo $query = RebuyModel::submitSecondQuote($order_id, $quote);
         }else{

             echo "please enter new offer";
         }
         ;
     }

     public function entersecondofferAction()
     {
         $order_id=$this->activateAction();
         $_SESSION['order_id']=$order_id;
         $results=RebuyModel::retrievefirstoffer($order_id);
         View::renderTemplate('Rebuy/entersecondoffer.html',[
             'results' => $results
         ]);


     }

     public function sendsecofferAction()
     {
         $order_id=$_SESSION['order_id'];
        $results=RebuyModel::retrievesecondquote($order_id);
         View::renderTemplate('Rebuy/sendsecoffer.html',[
             'results' => $results
         ]);

     }

     public function ftstatusreportsAction()
     {
         



     }

     public function offersAction()
     {
        $results=RebuyModel::getOffers();
         View::renderTemplate('rebuy/offers.html'
             ,[
             'results' => $results
         ]
         );


     }




}
















