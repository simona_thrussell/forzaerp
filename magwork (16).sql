-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2018 at 04:31 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.1.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `magwork`
--

-- --------------------------------------------------------

--
-- Table structure for table `forzaep_rebuy_device_quote_table`
--

CREATE TABLE `forzaep_rebuy_device_quote_table` (
  `quote_id` int(15) NOT NULL,
  `device_type_id` int(15) NOT NULL,
  `device_condition_id` int(15) NOT NULL,
  `device_connection_id` int(15) NOT NULL,
  `device_capacity` int(15) NOT NULL,
  `device_quote` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaep_rebuy_device_quote_table`
--

INSERT INTO `forzaep_rebuy_device_quote_table` (`quote_id`, `device_type_id`, `device_condition_id`, `device_connection_id`, `device_capacity`, `device_quote`) VALUES
(1, 1, 1, 3, 1, 40),
(2, 1, 1, 3, 2, 40),
(3, 1, 1, 3, 3, 40),
(4, 1, 4, 3, 1, 55),
(5, 1, 4, 3, 2, 60),
(6, 1, 4, 3, 3, 65),
(7, 1, 3, 3, 1, 70),
(8, 1, 3, 3, 2, 80),
(9, 1, 3, 3, 3, 90),
(10, 1, 2, 3, 1, 100),
(11, 1, 2, 3, 2, 110),
(12, 1, 2, 3, 3, 120),
(13, 2, 1, 3, 1, 35),
(14, 2, 1, 3, 3, 35),
(15, 2, 4, 3, 1, 50),
(16, 2, 4, 3, 3, 60),
(17, 2, 3, 3, 1, 75),
(18, 2, 3, 3, 3, 85),
(19, 2, 2, 3, 1, 100),
(20, 2, 2, 3, 3, 110);

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_action_status`
--

CREATE TABLE `forzaerp_action_status` (
  `action_id` int(15) NOT NULL,
  `action_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_action_status`
--

INSERT INTO `forzaerp_action_status` (`action_id`, `action_name`) VALUES
(1, 'WAIT FOR DEVICE TO BE RECEIVED'),
(2, 'CHECK DEVICE'),
(3, 'INFORM POLICE DEVICE STOLEN'),
(4, 'INFORM CUSTOMER CHECK NOT PASSED'),
(5, 'RE-CHECK DEVICE'),
(6, 'SEND TO INSPECTION'),
(7, 'QUOTE DEVICE'),
(8, 'SEND QUOTE'),
(9, 'SEND FOR PAYMENT'),
(10, 'SEND SECOND QUOTE'),
(11, 'RETURN DEVICE'),
(12, 'RECYCLE DEVICE'),
(13, 'CLOSE ORDER');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_connection_type`
--

CREATE TABLE `forzaerp_connection_type` (
  `connection_type_id` int(15) NOT NULL,
  `connection_type_name` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_connection_type`
--

INSERT INTO `forzaerp_connection_type` (`connection_type_id`, `connection_type_name`) VALUES
(1, 'WIFI'),
(2, 'WIFI + 4G'),
(3, 'N/A');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_customer`
--

CREATE TABLE `forzaerp_customer` (
  `customer_id` int(15) NOT NULL,
  `customer_first_name` varchar(155) NOT NULL,
  `customer_last_name` varchar(155) NOT NULL,
  `customer_email` varchar(255) NOT NULL,
  `customer_phone_no` int(15) NOT NULL,
  `customer_type` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_customer`
--

INSERT INTO `forzaerp_customer` (`customer_id`, `customer_first_name`, `customer_last_name`, `customer_email`, `customer_phone_no`, `customer_type`) VALUES
(1, 'Jan', 'Jansen', 'janjansen@hotmail.com', 0, 1),
(2, 'Ben', 'Scheepers', 'nightandtime@gmail.com', 0, 1),
(3, 'Lisa', 'Henegawen', 'liseh@gmail.com', 0, 1),
(4, 'John', 'Maverick', 'johnm@gmail.com', 0, 1),
(5, 'john', 'marrin', 'johnm@marrin.com', 0, 1),
(6, 'joe', 'thrussell', 'multiple.awareness@hotmail.com', 0, 1),
(7, 'joe', 'bloggers', 'multiple.awareness@hotmail.com', 0, 1),
(8, 'joe', 'dsgddg', 'multiple.awareness@hotmail.com', 0, 1),
(9, 'simona', 'thrussell', 'multiple.awareness@hotmail.com', 0, 1),
(10, 'ytjk', 'kyryrtr', '', 0, 1),
(11, 'Sam', 'Boddaert', 'multiple.awareness@hotmail.com', 0, 1),
(12, 'bfs', 'hffhfdh', 'hfadsfds@gmail.com', 0, 1),
(13, 'jrjrjerjereje', 'wbrshaerrj', 'hfadsfds@gmail.com', 0, 1),
(14, 'jrjrjerjereje', 'wbrshaerrjfbf', 'fbffhfadsfds@gmail.com', 0, 1),
(15, 'jrjrjerjereje', 'wbrshaerrjfbf', 'fbffhfadsfds@gmail.com', 0, 1),
(16, 'ytjk', 'kyryrtr', '', 0, 1),
(17, 'ytjk', 'kyryrtr', '', 0, 1),
(18, 'ytjkngfddf', 'kyryrtr', '', 0, 1),
(19, 'ytjkngfddf', 'kyryrtr', '', 0, 1),
(20, 'ytjkngfddf', 'kyryrtr', '', 0, 1),
(21, 'ytjkngfddf', 'kyryrtr', '', 0, 1),
(22, 'ytjkngfddf', 'kyryrtr', '', 0, 1),
(23, 'lora', 'keen', 'lorakeen@hotmail.com', 0, 1),
(24, 'axs bv', 'csnns n ss', 'multiple.awareness@hotmail.com', 0, 1),
(25, 'axs bv', 'csnns n ss', 'multiple.awareness@hotmail.com', 0, 1),
(26, 'axs bv', 'csnns n ss', 'multiple.awareness@hotmail.com', 0, 1),
(27, 'axs bv', 'csnns n ss', 'multiple.awareness@hotmail.com', 0, 1),
(28, 'xbxbx', 'xbbxb', '', 0, 1),
(29, '', '', '', 0, 1),
(30, 'Sam', 'Boddaert', 'multiple.awareness@hotmail.com', 615370063, 1);

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_customer_address`
--

CREATE TABLE `forzaerp_customer_address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(15) NOT NULL,
  `street_no` int(15) NOT NULL,
  `addition` varchar(255) NOT NULL,
  `street_name` varchar(255) NOT NULL,
  `postcode` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_customer_address`
--

INSERT INTO `forzaerp_customer_address` (`address_id`, `customer_id`, `street_no`, `addition`, `street_name`, `postcode`, `city`, `country`) VALUES
(1, 4, 28, 'n/a', 'mergelsweg', '2846 DD', 'Heerlen', 'NL'),
(0, 9, 0, '', 'kruidenlaan 395', '5044CJ', 'tilburg', 'Germany'),
(0, 10, 0, '', '', '', '', ''),
(0, 11, 0, '', 'Lochtingstraat 51A', '9940', 'Evergem', 'Belgium'),
(0, 12, 33, '', 'graaf engelbertlaan', '4837 DS', 'breda', 'Netherlands'),
(0, 13, 33, '', 'graaf engelbertlaan', '4837 DS', 'breda', 'Netherlands'),
(0, 14, 33, '', 'graaf engelbertlaan', '4837 DS', 'breda', 'Netherlands'),
(0, 15, 33, '', 'graaf engelbertlaan', '4837 DS', 'breda', 'Netherlands'),
(0, 16, 0, '', '', '', '', ''),
(0, 17, 0, '', '', '', '', ''),
(0, 18, 0, '', 'ddnfnnd', 'nfdfndn', 'fdn', 'nfd'),
(0, 19, 0, '', 'ddnfnnd', 'nfdfndn', 'fdn', 'nfd'),
(0, 20, 0, '', 'ddnfnnd', 'nfdfndn', 'fdn', 'nfd'),
(0, 21, 0, '', 'ddnfnnd', 'nfdfndn', 'fdn', 'nfd'),
(0, 22, 0, '', 'ddnfnnd', 'nfdfndn', 'fdn', 'nfd'),
(0, 23, 22, '', 'graaf engelbertlaan', '5044CJ', 'tilburg', 'Netherlands'),
(0, 24, 395, '', 'kruidenlaan 395', '5044CJ', 'tilburg', 'Germany'),
(0, 25, 395, '', 'kruidenlaan 395', '5044CJ', 'tilburg', 'Germany'),
(0, 26, 395, '', 'kruidenlaan 395', '5044CJ', 'tilburg', 'Germany'),
(0, 27, 395, '', 'kruidenlaan 395', '5044CJ', 'tilburg', 'Germany'),
(0, 28, 0, 'vxx', 'zbcx', 'zxbzb', 'zxxzz', 'xzbzxz'),
(0, 29, 0, '', '', '', '', ''),
(0, 30, 0, '', 'Lochtingstraat 51A', '9940', 'Evergem', 'Belgium');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_customer_expected_quote`
--

CREATE TABLE `forzaerp_customer_expected_quote` (
  `order_id` int(11) NOT NULL,
  `quote` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_departments`
--

CREATE TABLE `forzaerp_departments` (
  `user_department_id` int(10) NOT NULL,
  `user_department_name` enum('IT','production','logistics','sales','cs','finance') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_departments`
--

INSERT INTO `forzaerp_departments` (`user_department_id`, `user_department_name`) VALUES
(1, 'IT'),
(2, 'production'),
(3, 'logistics'),
(4, 'sales'),
(5, 'cs'),
(6, 'finance');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_device_colour`
--

CREATE TABLE `forzaerp_device_colour` (
  `colour_id` int(15) NOT NULL,
  `colour_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_device_colour`
--

INSERT INTO `forzaerp_device_colour` (`colour_id`, `colour_name`) VALUES
(1, 'Jet Black'),
(2, 'Red'),
(3, 'Rose Gold'),
(4, 'Silver'),
(5, 'Black'),
(6, 'Matte Black'),
(7, 'Gold'),
(8, 'White'),
(9, 'Blue');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_device_storage_type`
--

CREATE TABLE `forzaerp_device_storage_type` (
  `storage_type_id` int(15) NOT NULL,
  `storage_type_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_device_storage_type`
--

INSERT INTO `forzaerp_device_storage_type` (`storage_type_id`, `storage_type_name`) VALUES
(1, '16 GB'),
(2, '32 GB'),
(3, '64 GB'),
(4, '128 Gb'),
(5, '256 GB');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_device_type`
--

CREATE TABLE `forzaerp_device_type` (
  `device_id` int(15) NOT NULL,
  `device_model` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_device_type`
--

INSERT INTO `forzaerp_device_type` (`device_id`, `device_model`) VALUES
(1, 'IPhone SE'),
(2, 'IPhone 6'),
(3, 'IPhone 6S'),
(4, 'IPhone 6S Plus'),
(5, 'IPhone 7'),
(6, 'IPhone 8'),
(7, 'IPad 4'),
(8, 'IPad Mini 1'),
(9, 'IPad Mini 2'),
(10, 'IPad Mini 3'),
(11, 'IPad Mini 4'),
(12, 'IPad Air 1'),
(13, 'IPad Air 2'),
(14, 'IPad 2017'),
(15, 'IPhone 7Plus'),
(16, 'IPhone 4S'),
(17, 'IPhone 5'),
(18, 'IPhone 5S'),
(19, 'IPhone X');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_event_state`
--

CREATE TABLE `forzaerp_event_state` (
  `event_state_id` int(15) NOT NULL,
  `event_state_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_event_state`
--

INSERT INTO `forzaerp_event_state` (`event_state_id`, `event_state_name`) VALUES
(1, 'started'),
(2, 'completed'),
(3, 'aborted');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_inspection_details`
--

CREATE TABLE `forzaerp_inspection_details` (
  `order_id` int(15) NOT NULL,
  `IMEI` int(15) NOT NULL,
  `front_camera_flex_cable` int(15) NOT NULL,
  `front_camera` int(15) NOT NULL,
  `front_mic` int(15) NOT NULL,
  `proximity_sensor` int(15) NOT NULL,
  `auto_brightness` int(15) NOT NULL,
  `ambient_light` int(15) NOT NULL,
  `front_speaker` int(15) NOT NULL,
  `touch_id` int(15) NOT NULL,
  `home_button` int(15) NOT NULL,
  `display_image_quality` int(15) NOT NULL,
  `display_multi_touch` int(15) NOT NULL,
  `physical_damage` int(15) NOT NULL,
  `power` int(15) NOT NULL,
  `battery` int(15) NOT NULL,
  `dock_connector` int(15) NOT NULL,
  `charging` int(15) NOT NULL,
  `microphone` int(15) NOT NULL,
  `headset_jack` int(15) NOT NULL,
  `rear_speaker` int(15) NOT NULL,
  `vibration` int(15) NOT NULL,
  `power_flex_cable` int(15) NOT NULL,
  `power_button` int(15) NOT NULL,
  `microphone_back` int(15) NOT NULL,
  `torch` int(15) NOT NULL,
  `rear_camera` int(15) NOT NULL,
  `volume_flex_cable` int(15) NOT NULL,
  `SIM_fail` int(15) NOT NULL,
  `No_Connection` int(15) NOT NULL,
  `Signal_Strength` int(15) NOT NULL,
  `Wifi` int(15) NOT NULL,
  `Bluetooth` int(15) NOT NULL,
  `GPS` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_order`
--

CREATE TABLE `forzaerp_order` (
  `order_id` int(15) NOT NULL,
  `customer_id` int(15) NOT NULL,
  `order_date` date NOT NULL,
  `payment_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_order`
--

INSERT INTO `forzaerp_order` (`order_id`, `customer_id`, `order_date`, `payment_type_id`) VALUES
(1, 1, '2018-09-27', 1),
(2, 1, '2018-09-26', 2),
(3, 3, '2018-10-01', 2),
(5, 11, '2018-11-07', 1),
(6, 12, '2018-11-07', 1),
(7, 13, '2018-11-07', 1),
(8, 14, '2018-11-07', 1),
(9, 15, '2018-11-07', 1),
(10, 16, '2018-11-07', 1),
(11, 17, '2018-11-07', 1),
(12, 18, '2018-11-07', 1),
(13, 19, '2018-11-07', 1),
(14, 20, '2018-11-07', 1),
(15, 21, '2018-11-07', 1),
(16, 22, '2018-11-07', 1),
(17, 23, '2018-11-07', 1),
(18, 24, '2018-11-08', 1),
(19, 25, '2018-11-08', 1),
(20, 26, '2018-11-08', 1),
(21, 27, '2018-11-08', 1),
(22, 28, '2018-11-10', 1),
(23, 29, '2018-11-15', 1),
(24, 30, '2018-11-16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_parts_inventory`
--

CREATE TABLE `forzaerp_parts_inventory` (
  `part_id` int(15) NOT NULL,
  `part_type` int(15) NOT NULL,
  `part_supplier` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_parts_suppliers`
--

CREATE TABLE `forzaerp_parts_suppliers` (
  `supplier_id` int(15) NOT NULL,
  `supplier_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_parts_suppliers`
--

INSERT INTO `forzaerp_parts_suppliers` (`supplier_id`, `supplier_name`) VALUES
(1, 'Jack'),
(2, 'Sandy'),
(3, 'Lenny'),
(4, 'Roy');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_parts_type`
--

CREATE TABLE `forzaerp_parts_type` (
  `part_type_id` int(15) NOT NULL,
  `device` int(15) NOT NULL,
  `part_type_name` varchar(150) NOT NULL,
  `supplier_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_customer_estimation_types`
--

CREATE TABLE `forzaerp_rebuy_customer_estimation_types` (
  `est_type_id` int(15) NOT NULL,
  `est_type_name` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_customer_estimation_types`
--

INSERT INTO `forzaerp_rebuy_customer_estimation_types` (`est_type_id`, `est_type_name`) VALUES
(1, 'NOT FUNCTIONAL'),
(2, '100% FUNCTIONAL LIKE NEW OR LIGHT SIGNS OF USE'),
(3, '100%  FUNCTIONAL, VISIBLE SIGNS OF USE');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_customer_payment`
--

CREATE TABLE `forzaerp_rebuy_customer_payment` (
  `cust_id` int(15) NOT NULL,
  `order_id` int(11) NOT NULL,
  `cust_iban` varchar(18) NOT NULL,
  `cust_iban_name` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_customer_payment`
--

INSERT INTO `forzaerp_rebuy_customer_payment` (`cust_id`, `order_id`, `cust_iban`, `cust_iban_name`) VALUES
(1, 1, '446677990', 'j Jansen');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_customer_status_type`
--

CREATE TABLE `forzaerp_rebuy_customer_status_type` (
  `cust_status_id` int(15) NOT NULL,
  `cust_status_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_customer_status_type`
--

INSERT INTO `forzaerp_rebuy_customer_status_type` (`cust_status_id`, `cust_status_name`) VALUES
(1, 'ORDER CREATED'),
(2, 'DEVICE RECEIVED'),
(3, 'DEVICE SHIPPED'),
(4, 'OFFER ACCEPTED'),
(5, 'OFFER REFUSED'),
(6, 'RECYCLE DEVICE'),
(7, 'SECOND OFFER ACCEPTED'),
(8, 'SECOND OFFER REFUSED'),
(9, 'TO BE PAID'),
(10, 'PAID'),
(11, 'ORDER CLOSED'),
(12, 'RETURN DEVICE');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_customer_type`
--

CREATE TABLE `forzaerp_rebuy_customer_type` (
  `cust_type_id` int(15) NOT NULL,
  `cust_type_name` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_customer_type`
--

INSERT INTO `forzaerp_rebuy_customer_type` (`cust_type_id`, `cust_type_name`) VALUES
(1, 'INDIVIDUAL'),
(2, 'BUSINESS');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_device_check`
--

CREATE TABLE `forzaerp_rebuy_device_check` (
  `order_id` int(15) NOT NULL,
  `IMEI` int(15) NOT NULL,
  `Simlock` tinyint(1) NOT NULL,
  `ICloud lock` tinyint(1) NOT NULL,
  `Stolen` tinyint(1) NOT NULL,
  `checked` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_device_check`
--

INSERT INTO `forzaerp_rebuy_device_check` (`order_id`, `IMEI`, `Simlock`, `ICloud lock`, `Stolen`, `checked`) VALUES
(1, 2147483647, 0, 0, 0, 1),
(17, 1122233344, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_device_condition`
--

CREATE TABLE `forzaerp_rebuy_device_condition` (
  `condition_id` int(15) NOT NULL,
  `condition_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_device_condition`
--

INSERT INTO `forzaerp_rebuy_device_condition` (`condition_id`, `condition_name`) VALUES
(1, 'NONFUNCTIONAL'),
(2, '100% FUNCTIONAL LIGHT USE'),
(3, '100%FUNCTIONAL VISIBLE USE'),
(4, 'SMALL REPAIR');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_forza_order_status_type`
--

CREATE TABLE `forzaerp_rebuy_forza_order_status_type` (
  `status_id` int(15) NOT NULL,
  `status_name` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_forza_order_status_type`
--

INSERT INTO `forzaerp_rebuy_forza_order_status_type` (`status_id`, `status_name`) VALUES
(1, 'ORDER RECEIVED'),
(2, 'LABEL SENT'),
(3, 'DEVICE RECEIVED'),
(4, 'CHECKED'),
(6, 'INSPECTION'),
(7, 'ACCEPTED'),
(8, 'NOT ACCEPTED'),
(9, 'OFFER SENT'),
(10, 'SECOND OFFER SENT'),
(11, 'TO BE PAID'),
(12, 'PAID'),
(13, 'DEVICE RETURNED'),
(14, 'DEVICE RECYCLED'),
(15, 'ORDER CLOSED'),
(16, 'INSPECTED'),
(17, 'QUOTED');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_forza_status_shipping`
--

CREATE TABLE `forzaerp_rebuy_forza_status_shipping` (
  `order_id` int(15) NOT NULL,
  `order_date` date NOT NULL,
  `order_shipping_status` varchar(15) NOT NULL,
  `order_received_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_inspection`
--

CREATE TABLE `forzaerp_rebuy_inspection` (
  `order_id` int(11) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `device_storage` varchar(255) NOT NULL,
  `device_connection` varchar(255) NOT NULL,
  `device_condition` varchar(255) NOT NULL,
  `device_colour` varchar(255) NOT NULL,
  `device_comments` text NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_inspection`
--

INSERT INTO `forzaerp_rebuy_inspection` (`order_id`, `device_type`, `device_storage`, `device_connection`, `device_condition`, `device_colour`, `device_comments`, `date`) VALUES
(1, '1', '1', '1', '1', '1', 'Enter comments here...', '0000-00-00'),
(2, '1', '1', '1', '1', '1', 'scratches', '0000-00-00'),
(3, '1', '1', '1', '1', '1', 'Enter comments here...', '0000-00-00'),
(14, '11', '4', '2', '2', '6', 'Enter comments here... bmbmm', '0000-00-00'),
(16, '1', '1', '1', '2', '3', 'Enter comments here...  n nnb..b..bmm.', '0000-00-00'),
(18, '1', '1', '1', '1', '1', 'Enter comments here...', '0000-00-00'),
(19, '5', '2', '2', '2', '3', 'Enter comments here...', '0000-00-00'),
(21, '1', '1', '1', '2', '6', 'Enter comments hez bcz c n cxre...', '0000-00-00'),
(23, '1', '1', '1', '1', '1', 'Enter comments here...', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_inspection_failcard`
--

CREATE TABLE `forzaerp_rebuy_inspection_failcard` (
  `order_id` int(15) NOT NULL,
  `device_IMEI` int(15) NOT NULL,
  `failcard` tinyint(1) NOT NULL,
  `battery` tinyint(1) NOT NULL,
  `speakers` tinyint(1) NOT NULL,
  `lcd` tinyint(1) NOT NULL,
  `camera` tinyint(1) NOT NULL,
  `microphone` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_inventory`
--

CREATE TABLE `forzaerp_rebuy_inventory` (
  `IMEI` int(20) NOT NULL,
  `order_id` int(15) NOT NULL,
  `device_type` int(15) NOT NULL,
  `device_condition` int(15) NOT NULL,
  `device_storage` int(15) NOT NULL,
  `device_connection` int(15) NOT NULL,
  `device_colour` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_order_device`
--

CREATE TABLE `forzaerp_rebuy_order_device` (
  `order_id` int(15) NOT NULL,
  `device_type_id` int(15) NOT NULL,
  `device_storage_id` int(15) NOT NULL,
  `device_condition_id` int(15) NOT NULL,
  `device_connection_id` int(15) NOT NULL,
  `device_colour_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_order_device`
--

INSERT INTO `forzaerp_rebuy_order_device` (`order_id`, `device_type_id`, `device_storage_id`, `device_condition_id`, `device_connection_id`, `device_colour_id`) VALUES
(1, 3, 2, 3, 3, 8),
(2, 12, 3, 3, 2, 4),
(3, 1, 1, 3, 3, 4),
(14, 3, 3, 1, 2, 1),
(16, 4, 5, 3, 2, 2),
(17, 5, 4, 2, 1, 4),
(18, 1, 1, 2, 1, 3),
(19, 1, 1, 2, 1, 3),
(20, 1, 1, 2, 1, 3),
(21, 1, 1, 2, 1, 3),
(22, 1, 1, 1, 1, 1),
(23, 1, 1, 1, 1, 1),
(24, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_order_overview`
--

CREATE TABLE `forzaerp_rebuy_order_overview` (
  `order_id` int(15) NOT NULL,
  `customer_id` int(15) NOT NULL,
  `customer_status` int(15) NOT NULL,
  `forza_status` int(15) NOT NULL,
  `last_action_date` date NOT NULL,
  `order_quote` float NOT NULL,
  `order_tag_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_order_quote`
--

CREATE TABLE `forzaerp_rebuy_order_quote` (
  `order_id` int(11) NOT NULL,
  `order_quote` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_order_quote`
--

INSERT INTO `forzaerp_rebuy_order_quote` (`order_id`, `order_quote`) VALUES
(1, 12),
(2, 55),
(3, 55),
(23, 22);

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_order_salestag`
--

CREATE TABLE `forzaerp_rebuy_order_salestag` (
  `order_id` int(15) NOT NULL,
  `sales_tag_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_order_salestag`
--

INSERT INTO `forzaerp_rebuy_order_salestag` (`order_id`, `sales_tag_id`) VALUES
(2, 1),
(1, 2),
(3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_order_secondquote`
--

CREATE TABLE `forzaerp_rebuy_order_secondquote` (
  `order_id` int(15) NOT NULL,
  `second_quote` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_order_secondquote`
--

INSERT INTO `forzaerp_rebuy_order_secondquote` (`order_id`, `second_quote`) VALUES
(2, 65);

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_order_status`
--

CREATE TABLE `forzaerp_rebuy_order_status` (
  `order_id` int(15) NOT NULL,
  `forza_order_status` int(15) NOT NULL,
  `customer_order_status` int(15) NOT NULL,
  `next_action_id` int(15) NOT NULL,
  `date_last_action` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_order_status`
--

INSERT INTO `forzaerp_rebuy_order_status` (`order_id`, `forza_order_status`, `customer_order_status`, `next_action_id`, `date_last_action`) VALUES
(1, 3, 2, 10, '2018-10-03 16:44:59'),
(2, 9, 5, 11, '2018-10-03 17:03:02'),
(3, 17, 3, 6, '2018-10-03 17:11:35'),
(17, 1, 1, 1, '2018-11-07 17:25:32'),
(18, 1, 1, 1, '2018-11-08 17:02:00'),
(19, 3, 1, 1, '2018-11-08 17:02:35'),
(20, 1, 1, 1, '2018-11-08 17:03:27'),
(21, 1, 1, 1, '2018-11-08 17:11:41'),
(22, 1, 1, 1, '2018-11-10 11:22:47'),
(23, 17, 1, 1, '2018-11-15 11:38:46'),
(24, 3, 1, 1, '2018-11-16 09:51:06');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_payment_action`
--

CREATE TABLE `forzaerp_rebuy_payment_action` (
  `order_id` int(15) NOT NULL,
  `customer_id` int(15) NOT NULL,
  `payment_type_id` int(15) NOT NULL,
  `payment_amount` float NOT NULL,
  `payment_date` date NOT NULL,
  `payment status` enum('Paid','Awaiting Payment','Store Credit Issued') NOT NULL,
  `worker_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_payment_types`
--

CREATE TABLE `forzaerp_rebuy_payment_types` (
  `payment_type_id` int(15) NOT NULL,
  `payment_type` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_payment_types`
--

INSERT INTO `forzaerp_rebuy_payment_types` (`payment_type_id`, `payment_type`) VALUES
(1, 'IBAN'),
(2, 'Forza Credit');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_shipping`
--

CREATE TABLE `forzaerp_rebuy_shipping` (
  `order_id` int(15) NOT NULL,
  `shipping_status` int(15) NOT NULL,
  `shipping_date` date NOT NULL,
  `received_date` date DEFAULT NULL,
  `returned_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_shipping`
--

INSERT INTO `forzaerp_rebuy_shipping` (`order_id`, `shipping_status`, `shipping_date`, `received_date`, `returned_date`) VALUES
(1, 3, '2018-09-27', '0000-00-00', '0000-00-00'),
(2, 3, '2018-09-27', '0000-00-00', '0000-00-00'),
(21, 7, '0000-00-00', NULL, NULL),
(22, 7, '0000-00-00', NULL, NULL),
(23, 3, '0000-00-00', NULL, NULL),
(24, 3, '0000-00-00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_rebuy_shipping_status`
--

CREATE TABLE `forzaerp_rebuy_shipping_status` (
  `shipping_status_id` int(15) NOT NULL,
  `shipping_status_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_rebuy_shipping_status`
--

INSERT INTO `forzaerp_rebuy_shipping_status` (`shipping_status_id`, `shipping_status_name`) VALUES
(1, 'DEVICE SENT'),
(2, 'SHIPPED'),
(3, 'RECEIVED'),
(4, 'SHIPPED TO CUSTOMER'),
(5, 'RECEIVED BY CUSTOMER'),
(6, 'LOST IN TRANSIT'),
(7, 'WAITING FOR SHIPPING');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_sales_order_tags`
--

CREATE TABLE `forzaerp_sales_order_tags` (
  `tag_id` int(15) NOT NULL,
  `tag_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_sales_order_tags`
--

INSERT INTO `forzaerp_sales_order_tags` (`tag_id`, `tag_name`) VALUES
(1, 'Exchange'),
(2, 'Marleen'),
(4, 'Dave'),
(5, 'Pepijn'),
(6, 'Jorge'),
(7, 'Jeroen-Raatgever'),
(8, 'Pieter Keijzer'),
(9, 'Newten Melcherts'),
(10, 'Vera Geerling'),
(11, 'Michael Pique'),
(12, 'Martin Prenen'),
(13, 'femke');

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_users`
--

CREATE TABLE `forzaerp_users` (
  `user_id` int(10) NOT NULL,
  `user_name` varchar(150) NOT NULL,
  `user_email` varchar(150) NOT NULL,
  `user_password` varchar(150) NOT NULL,
  `user_department` int(10) NOT NULL,
  `user_role` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_users`
--

INSERT INTO `forzaerp_users` (`user_id`, `user_name`, `user_email`, `user_password`, `user_department`, `user_role`) VALUES
(2, 'simona', 'simona.thrussell@forza.refurbished.nl', 'darken47', 1, 4),
(3, 'juriel', 'juriel.verbaarschot@forza-refurbished.nl', 'Forza123!', 1, 4),
(4, 'marijn', 'marijn.schouw@forza-refurbished.nl', 'Forza123!', 2, 4),
(5, 'Armen', 'armen.albert@forza-refurbished.nl', 'Forza123!', 2, 4),
(6, 'Jeffrey', 'jeffrey.segeren@forza-refurbished.nl', 'Forza123!', 2, 3),
(7, 'Maarten', 'maarten.been@forza-refurbished.nl', 'Forza123!', 2, 6),
(8, 'Janos', 'janos.czeh@forza-group.nl', 'Forza123!', 2, 6),
(9, 'Magda', 'Magda.Filipek@forza-refurbished.nl', 'Forza123!', 2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `forzaerp_user_roles`
--

CREATE TABLE `forzaerp_user_roles` (
  `user_role_id` int(10) NOT NULL,
  `user_role_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzaerp_user_roles`
--

INSERT INTO `forzaerp_user_roles` (`user_role_id`, `user_role_name`) VALUES
(1, 'director'),
(2, 'supervisor'),
(3, 'manager'),
(4, 'administrator'),
(5, 'salesworker'),
(6, 'repair tech'),
(7, 'IT office'),
(8, 'support desk'),
(9, 'finance clerk'),
(10, 'warehouse op'),
(11, 'developer');

-- --------------------------------------------------------

--
-- Table structure for table `forzerp_event_type`
--

CREATE TABLE `forzerp_event_type` (
  `event_type_id` int(15) NOT NULL,
  `event_type` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forzerp_event_type`
--

INSERT INTO `forzerp_event_type` (`event_type_id`, `event_type`) VALUES
(1, 'Check'),
(2, 'Inspection'),
(3, 'Repair 1'),
(4, 'Repair 2'),
(5, 'Call- Sales'),
(6, 'Call-CS'),
(7, 'Call-RMA'),
(8, 'Payment');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `forzaep_rebuy_device_quote_table`
--
ALTER TABLE `forzaep_rebuy_device_quote_table`
  ADD PRIMARY KEY (`quote_id`),
  ADD KEY `device_type_id` (`device_type_id`),
  ADD KEY `device_condition_id` (`device_condition_id`),
  ADD KEY `device_connection_id` (`device_connection_id`);

--
-- Indexes for table `forzaerp_action_status`
--
ALTER TABLE `forzaerp_action_status`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `action_id` (`action_id`);

--
-- Indexes for table `forzaerp_connection_type`
--
ALTER TABLE `forzaerp_connection_type`
  ADD PRIMARY KEY (`connection_type_id`);

--
-- Indexes for table `forzaerp_customer`
--
ALTER TABLE `forzaerp_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `forzaerp_departments`
--
ALTER TABLE `forzaerp_departments`
  ADD PRIMARY KEY (`user_department_id`);

--
-- Indexes for table `forzaerp_device_colour`
--
ALTER TABLE `forzaerp_device_colour`
  ADD PRIMARY KEY (`colour_id`);

--
-- Indexes for table `forzaerp_device_storage_type`
--
ALTER TABLE `forzaerp_device_storage_type`
  ADD PRIMARY KEY (`storage_type_id`);

--
-- Indexes for table `forzaerp_device_type`
--
ALTER TABLE `forzaerp_device_type`
  ADD PRIMARY KEY (`device_id`);

--
-- Indexes for table `forzaerp_event_state`
--
ALTER TABLE `forzaerp_event_state`
  ADD KEY `event_state_id` (`event_state_id`);

--
-- Indexes for table `forzaerp_order`
--
ALTER TABLE `forzaerp_order`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `order_date` (`order_date`);

--
-- Indexes for table `forzaerp_parts_suppliers`
--
ALTER TABLE `forzaerp_parts_suppliers`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `forzaerp_parts_type`
--
ALTER TABLE `forzaerp_parts_type`
  ADD PRIMARY KEY (`part_type_id`),
  ADD KEY `device` (`device`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `forzaerp_rebuy_customer_estimation_types`
--
ALTER TABLE `forzaerp_rebuy_customer_estimation_types`
  ADD PRIMARY KEY (`est_type_id`);

--
-- Indexes for table `forzaerp_rebuy_customer_payment`
--
ALTER TABLE `forzaerp_rebuy_customer_payment`
  ADD PRIMARY KEY (`cust_id`);

--
-- Indexes for table `forzaerp_rebuy_customer_status_type`
--
ALTER TABLE `forzaerp_rebuy_customer_status_type`
  ADD PRIMARY KEY (`cust_status_id`);

--
-- Indexes for table `forzaerp_rebuy_customer_type`
--
ALTER TABLE `forzaerp_rebuy_customer_type`
  ADD PRIMARY KEY (`cust_type_id`);

--
-- Indexes for table `forzaerp_rebuy_device_check`
--
ALTER TABLE `forzaerp_rebuy_device_check`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `forzaerp_rebuy_device_condition`
--
ALTER TABLE `forzaerp_rebuy_device_condition`
  ADD PRIMARY KEY (`condition_id`),
  ADD KEY `condition_id` (`condition_id`);

--
-- Indexes for table `forzaerp_rebuy_forza_order_status_type`
--
ALTER TABLE `forzaerp_rebuy_forza_order_status_type`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `forzaerp_rebuy_forza_status_shipping`
--
ALTER TABLE `forzaerp_rebuy_forza_status_shipping`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `forzaerp_rebuy_inspection`
--
ALTER TABLE `forzaerp_rebuy_inspection`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `forzaerp_rebuy_inspection_failcard`
--
ALTER TABLE `forzaerp_rebuy_inspection_failcard`
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `forzaerp_rebuy_inventory`
--
ALTER TABLE `forzaerp_rebuy_inventory`
  ADD KEY `order_id` (`order_id`),
  ADD KEY `IMEI` (`IMEI`);

--
-- Indexes for table `forzaerp_rebuy_order_device`
--
ALTER TABLE `forzaerp_rebuy_order_device`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `device_type_id` (`device_type_id`),
  ADD KEY `device_storage_id` (`device_storage_id`),
  ADD KEY `device_colour_id` (`device_colour_id`),
  ADD KEY `device_connection_id` (`device_connection_id`),
  ADD KEY `device_condition_id` (`device_condition_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `forzaerp_rebuy_order_overview`
--
ALTER TABLE `forzaerp_rebuy_order_overview`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `customer_status` (`customer_status`),
  ADD KEY `forza_status` (`forza_status`),
  ADD KEY `order_tag_id` (`order_tag_id`);

--
-- Indexes for table `forzaerp_rebuy_order_quote`
--
ALTER TABLE `forzaerp_rebuy_order_quote`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `forzaerp_rebuy_order_salestag`
--
ALTER TABLE `forzaerp_rebuy_order_salestag`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `sales_tag_id` (`sales_tag_id`);

--
-- Indexes for table `forzaerp_rebuy_order_secondquote`
--
ALTER TABLE `forzaerp_rebuy_order_secondquote`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `forzaerp_rebuy_order_status`
--
ALTER TABLE `forzaerp_rebuy_order_status`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `forza_order_status` (`forza_order_status`),
  ADD KEY `customer_order_status` (`customer_order_status`),
  ADD KEY `next_action_id` (`next_action_id`);

--
-- Indexes for table `forzaerp_rebuy_payment_action`
--
ALTER TABLE `forzaerp_rebuy_payment_action`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `forzaerp_rebuy_payment_types`
--
ALTER TABLE `forzaerp_rebuy_payment_types`
  ADD PRIMARY KEY (`payment_type_id`);

--
-- Indexes for table `forzaerp_rebuy_shipping`
--
ALTER TABLE `forzaerp_rebuy_shipping`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `shipping_status` (`shipping_status`);

--
-- Indexes for table `forzaerp_rebuy_shipping_status`
--
ALTER TABLE `forzaerp_rebuy_shipping_status`
  ADD PRIMARY KEY (`shipping_status_id`);

--
-- Indexes for table `forzaerp_sales_order_tags`
--
ALTER TABLE `forzaerp_sales_order_tags`
  ADD PRIMARY KEY (`tag_id`),
  ADD KEY `tag_id` (`tag_id`);

--
-- Indexes for table `forzaerp_users`
--
ALTER TABLE `forzaerp_users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_department` (`user_department`),
  ADD KEY `user_role` (`user_role`),
  ADD KEY `user_department_2` (`user_department`),
  ADD KEY `user_role_2` (`user_role`);

--
-- Indexes for table `forzaerp_user_roles`
--
ALTER TABLE `forzaerp_user_roles`
  ADD PRIMARY KEY (`user_role_id`);

--
-- Indexes for table `forzerp_event_type`
--
ALTER TABLE `forzerp_event_type`
  ADD PRIMARY KEY (`event_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `forzaep_rebuy_device_quote_table`
--
ALTER TABLE `forzaep_rebuy_device_quote_table`
  MODIFY `quote_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `forzaerp_action_status`
--
ALTER TABLE `forzaerp_action_status`
  MODIFY `action_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `forzaerp_connection_type`
--
ALTER TABLE `forzaerp_connection_type`
  MODIFY `connection_type_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `forzaerp_customer`
--
ALTER TABLE `forzaerp_customer`
  MODIFY `customer_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `forzaerp_departments`
--
ALTER TABLE `forzaerp_departments`
  MODIFY `user_department_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `forzaerp_device_type`
--
ALTER TABLE `forzaerp_device_type`
  MODIFY `device_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `forzaerp_event_state`
--
ALTER TABLE `forzaerp_event_state`
  MODIFY `event_state_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `forzaerp_order`
--
ALTER TABLE `forzaerp_order`
  MODIFY `order_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `forzaerp_parts_suppliers`
--
ALTER TABLE `forzaerp_parts_suppliers`
  MODIFY `supplier_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `forzaerp_rebuy_customer_estimation_types`
--
ALTER TABLE `forzaerp_rebuy_customer_estimation_types`
  MODIFY `est_type_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `forzaerp_rebuy_customer_status_type`
--
ALTER TABLE `forzaerp_rebuy_customer_status_type`
  MODIFY `cust_status_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `forzaerp_rebuy_customer_type`
--
ALTER TABLE `forzaerp_rebuy_customer_type`
  MODIFY `cust_type_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `forzaerp_rebuy_forza_order_status_type`
--
ALTER TABLE `forzaerp_rebuy_forza_order_status_type`
  MODIFY `status_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `forzaerp_rebuy_shipping_status`
--
ALTER TABLE `forzaerp_rebuy_shipping_status`
  MODIFY `shipping_status_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `forzaerp_sales_order_tags`
--
ALTER TABLE `forzaerp_sales_order_tags`
  MODIFY `tag_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `forzaerp_users`
--
ALTER TABLE `forzaerp_users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `forzerp_event_type`
--
ALTER TABLE `forzerp_event_type`
  MODIFY `event_type_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `forzaep_rebuy_device_quote_table`
--
ALTER TABLE `forzaep_rebuy_device_quote_table`
  ADD CONSTRAINT `c2` FOREIGN KEY (`device_type_id`) REFERENCES `forzaerp_device_type` (`device_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `forzaerp_rebuy_device_check`
--
ALTER TABLE `forzaerp_rebuy_device_check`
  ADD CONSTRAINT `o8` FOREIGN KEY (`order_id`) REFERENCES `forzaerp_order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `forzaerp_rebuy_inventory`
--
ALTER TABLE `forzaerp_rebuy_inventory`
  ADD CONSTRAINT `o2` FOREIGN KEY (`order_id`) REFERENCES `forzaerp_order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `forzaerp_rebuy_order_device`
--
ALTER TABLE `forzaerp_rebuy_order_device`
  ADD CONSTRAINT `c3` FOREIGN KEY (`device_connection_id`) REFERENCES `forzaerp_connection_type` (`connection_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `c4` FOREIGN KEY (`device_colour_id`) REFERENCES `forzaerp_device_colour` (`colour_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `c5` FOREIGN KEY (`device_condition_id`) REFERENCES `forzaerp_rebuy_customer_estimation_types` (`est_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `c7` FOREIGN KEY (`device_condition_id`) REFERENCES `forzaerp_rebuy_device_condition` (`condition_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `o3` FOREIGN KEY (`order_id`) REFERENCES `forzaerp_order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `s3` FOREIGN KEY (`device_storage_id`) REFERENCES `forzaerp_device_storage_type` (`storage_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `t3` FOREIGN KEY (`device_type_id`) REFERENCES `forzaerp_device_type` (`device_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `forzaerp_rebuy_order_overview`
--
ALTER TABLE `forzaerp_rebuy_order_overview`
  ADD CONSTRAINT `t4` FOREIGN KEY (`order_tag_id`) REFERENCES `forzaerp_sales_order_tags` (`tag_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `forzaerp_rebuy_order_quote`
--
ALTER TABLE `forzaerp_rebuy_order_quote`
  ADD CONSTRAINT `ii` FOREIGN KEY (`order_id`) REFERENCES `forzaerp_order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `forzaerp_rebuy_order_salestag`
--
ALTER TABLE `forzaerp_rebuy_order_salestag`
  ADD CONSTRAINT `st` FOREIGN KEY (`order_id`) REFERENCES `forzaerp_order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `st2` FOREIGN KEY (`sales_tag_id`) REFERENCES `forzaerp_sales_order_tags` (`tag_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `forzaerp_users`
--
ALTER TABLE `forzaerp_users`
  ADD CONSTRAINT `udep` FOREIGN KEY (`user_department`) REFERENCES `forzaerp_departments` (`user_department_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `urole` FOREIGN KEY (`user_role`) REFERENCES `forzaerp_user_roles` (`user_role_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
